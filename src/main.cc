/* © Mikhail Gozhev <dev@gozhev.ru> / November 2018 / Moscow, Russia */

/*
 * ВНИМАНИЕ! ГОВНОКОД
 * Эта программа написана в формате 'скетч'. Много длинных функций
 * и глобальных переменных.
 */

#include <iostream>
#include <iomanip>
#include <sstream>
#include <cassert>
#include <vector>
#include <cmath>
#include <exception>
#include <memory>
#include <thread>
#include <atomic>
#include <map>
#include <cstdint>
#include <condition_variable>
#include <mutex>
#include <chrono>

#include <GL/glew.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <opencv2/opencv.hpp>

#include <time.h>

#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "scope_guard.hh"
#include "load_shaders.hh"

#define CAMERA_BUFFERS_COUNT 20

void imgproc_worker(time_t sec0);
void cam_worker(time_t sec0);
int xioctl(int fd, unsigned long request, void *arg);

constexpr unsigned pixelformat_of(char a, char b, char c, char d)
{
	return (a << 0) | (b << 8) | (c << 16) | (d << 24);
}

char const *default_video_device = "/dev/video2";
char const *default_digit_texture = "data/digits.png";

unsigned constexpr default_pixelformat = pixelformat_of('G','R','E','Y');
unsigned const default_resolution_width = 1280;
unsigned const default_resolution_height = 960;
unsigned const default_fps = 45;

int const default_digit_width = 60;
int const default_digit_height = 100;

char const *vertex_shader = R"glsl(
#version 330 core
layout(location = 0) in vec2 xy;
out vec2 _xy;
void main()
{
	gl_Position = vec4(xy.x*2.f/7.f - 1.f, xy.y*2.f - 1.f, 0.f, 1.f);
	_xy = xy;
}
)glsl";

char const *fragment_shader = R"glsl(
#version 330 core
out vec4 color;
uniform sampler2D tex;
uniform int map[7];
in vec2 _xy;
void main()
{
	int n = int(_xy.x);
	vec2 uv = vec2((_xy.x - n + map[n])/10.f, 1.0 - _xy.y);
	color = texture(tex, uv);
}
)glsl";

std::atomic_bool g_quit{false};

uint8_t *g_shared_buf{nullptr};
struct timeval g_shbuf_ts;

enum class imgproc_signal { none, quit, process };
imgproc_signal g_ipsig = imgproc_signal::none;
std::condition_variable g_ipsig_cv;
std::mutex g_ipsig_mtx;

int main(int, char **) try
{
	int ret;

	ret = glfwInit();
	if (!ret) {
		throw std::runtime_error("failed to initialize GLFW");
	}
	auto glfw_guard = ON_SCOPE_EXIT() {
		glfwTerminate();
	};

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 0);

	GLFWwindow *win = glfwCreateWindow(7*default_digit_width,
			default_digit_height, "counter", nullptr, nullptr);
	if (!win) {
		throw std::runtime_error("failed to create GLFW window");
	}
	auto win_guard = ON_SCOPE_EXIT() {
		glfwDestroyWindow(win);
	};

	glfwSetInputMode(win, GLFW_STICKY_KEYS, GL_TRUE);
	glfwMakeContextCurrent(win);

	glewExperimental = true;
	ret = glewInit();
	if (ret != GLEW_OK) {
		throw std::runtime_error("failed to initialize GLEW");
	}

	GLuint pgm_id = load_shaders(vertex_shader, fragment_shader);
	if (!pgm_id) {
		throw std::runtime_error("failed to load shaders");
	}
	auto pgm_guard = ON_SCOPE_EXIT() {
		glDeleteProgram(pgm_id);
	};

	glUseProgram(pgm_id);
	glEnable(GL_TEXTURE_2D);

	GLuint vtxarr_id;
	glGenVertexArrays(1, &vtxarr_id);
	auto vtxarr_guard = ON_SCOPE_EXIT() {
		glDeleteVertexArrays(1, &vtxarr_id);
	};
	glBindVertexArray(vtxarr_id);

	std::vector<GLfloat> vtxbuf_data(2*2*8, 0.f);
	{
		auto i = vtxbuf_data.begin();
		for (size_t n = 0; n < vtxbuf_data.size()/4; ++n) {
			*i++ = (float) n;
			*i++ = 0.f;
			*i++ = (float) n;
			*i++ = 1.f;
		}
	}

	GLuint vtxbuf_id;
	glGenBuffers(1, &vtxbuf_id);
	auto vtxbuf_guard = ON_SCOPE_EXIT() {
		glDeleteBuffers(1, &vtxbuf_id);
	};
	glBindBuffer(GL_ARRAY_BUFFER, vtxbuf_id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vtxbuf_data.size(),
			vtxbuf_data.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);

	GLuint tex_id;
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &tex_id);
	auto tex_guard = ON_SCOPE_EXIT() {
		glDeleteTextures(1, &tex_id);
	};
	glBindTexture(GL_TEXTURE_2D, tex_id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	GLuint const idxbuf_data[2][8] = {
		{ 0, 1, 2, 3, 4, 5, 6, 7 },
		{ 8, 9, 10, 11, 12, 13, 14, 15 }
	};

	std::vector<GLuint> idxbuf_id(2);
	glGenBuffers((int)idxbuf_id.size(), idxbuf_id.data());
	auto idxbuf_guard = ON_SCOPE_EXIT() {
		glDeleteBuffers((int)idxbuf_id.size(), idxbuf_id.data());
	};
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxbuf_id[0]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idxbuf_data[0]),
			idxbuf_data[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxbuf_id[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idxbuf_data[1]),
			idxbuf_data[1], GL_STATIC_DRAW);

	GLint map_loc;
	map_loc = glGetUniformLocation(pgm_id, "map");
	if (map_loc < 0) {
		throw std::runtime_error(
				"failed to get 'map' uniform location");
	}

	{
		cv::Mat img = cv::imread(default_digit_texture);
		if (img.empty()) {
			throw std::runtime_error(
					"failed to read texture image");
		}
		if (img.cols != default_digit_width*10
				|| img.rows != default_digit_height)
		{
			throw std::runtime_error(
					"texture image dimensions doesn't match the hardcoded ones"
					);
		}

		glActiveTexture(GL_TEXTURE0);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, img.cols, img.rows,
				0, GL_BGR, GL_UNSIGNED_BYTE, img.data);
	}

	/* maps digits -> texture places */
	int map[7] = {};

	time_t sec0;
	struct timespec ts;
	long sec;
	long msec;

	clock_gettime(CLOCK_MONOTONIC, &ts);
	sec0 = ts.tv_sec;

	std::thread cam_thread(cam_worker, sec0);
	auto cam_thread_guard = ON_SCOPE_EXIT() {
		cam_thread.join();
	};

	auto quit_guard = ON_SCOPE_EXIT() {
		g_quit = true;
	};

	glfwPollEvents();
	while (!g_quit && !glfwWindowShouldClose(win)
			&& glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS
			&& glfwGetKey(win, GLFW_KEY_Q) != GLFW_PRESS)
	{
		{
			clock_gettime(CLOCK_MONOTONIC, &ts);
			msec = ts.tv_nsec/1000000;
			sec = (long) (ts.tv_sec - sec0);

			if (sec > 999) {
				break;
			}
			for (int i = 2; i >= 0; --i) {
				map[i] = (int)(sec % 10);
				sec /= 10;
			}
			for (int i = 6; i >= 4; --i) {
				map[i] = (int)(msec % 10);
				msec /= 10;
			}
			glProgramUniform1iv(pgm_id, map_loc, 7, map);
		}

		glClear(GL_DEPTH_BUFFER_BIT);

		glUseProgram(pgm_id);
		glBindVertexArray(vtxarr_id);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxbuf_id[0]);
		glDrawElements(GL_TRIANGLE_STRIP, 8,
				GL_UNSIGNED_INT, (void *)0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxbuf_id[1]);
		glDrawElements(GL_TRIANGLE_STRIP, 8,
				GL_UNSIGNED_INT, (void *)0);

		glfwSwapBuffers(win);
		glfwPollEvents();
	}

	return 0;
}
catch (std::exception &e) {
	std::cerr << "error: " << e.what() << std::endl;
	return -1;
}

void cam_worker(time_t sec0) try
{
	int ret = 0;

	/*
	 * if something goes wrong signal all threads to quit
	 */
	auto quit_guard = ON_SCOPE_EXIT() {
		g_quit = true;
	};

	int fd = open(default_video_device, O_RDWR | O_NONBLOCK, 0);
	if (fd == -1) {
		throw std::runtime_error("failed to open video device");
	}
	auto fd_guard = ON_SCOPE_EXIT() {
		close(fd);
	};

	/*
	 * check camera capabilities
	 */

	struct v4l2_capability capability;
	memset(&capability, 0, sizeof(capability));

	if (xioctl(fd, VIDIOC_QUERYCAP, &capability) == -1)
		throw std::runtime_error("failed to obtain camera capability");

	if (!(capability.capabilities & V4L2_CAP_VIDEO_CAPTURE))
		throw std::runtime_error("camera doesn't support capturing");

	if (!(capability.capabilities & V4L2_CAP_STREAMING))
		throw std::runtime_error("camera doesn't support streaming");

	/*
	 * enumerate formats
	 */

	struct v4l2_fmtdesc fmtdesc;
	memset(&fmtdesc, 0, sizeof(fmtdesc));

	fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmtdesc.index = 0;

	while ((ret = xioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc)) != -1) {
		if (fmtdesc.pixelformat == default_pixelformat)
			break;
		++fmtdesc.index;
	}

	if (ret < 0)
		throw std::runtime_error("requested pixelformat not found");

	/*
	 * enumerate resolutions
	 */

	struct v4l2_frmsizeenum frmsizeenum;
	memset(&frmsizeenum, 0, sizeof(frmsizeenum));

	frmsizeenum.pixel_format = fmtdesc.pixelformat;
	frmsizeenum.index = 0;

	while ((ret = xioctl(fd, VIDIOC_ENUM_FRAMESIZES, &frmsizeenum)) != -1) {
		if (frmsizeenum.type == V4L2_FRMSIZE_TYPE_DISCRETE) {
			if (default_resolution_width ==
					frmsizeenum.discrete.width
					&& default_resolution_height ==
					frmsizeenum.discrete.height)
			{
				break;
			}
		} else if (frmsizeenum.type == V4L2_FRMSIZE_TYPE_STEPWISE) {
		} else if (frmsizeenum.type == V4L2_FRMSIZE_TYPE_CONTINUOUS) {
		} else {
		}

		++frmsizeenum.index;
	}

	if (ret < 0)
		throw std::runtime_error("requested resolution not found");

	/*
	 * enumerate frame rates
	 */

	struct v4l2_frmivalenum frmivalenum;
	memset(&frmivalenum, 0, sizeof(frmivalenum));

	frmivalenum.pixel_format = fmtdesc.pixelformat;
	frmivalenum.width = frmsizeenum.discrete.width;
	frmivalenum.height = frmsizeenum.discrete.height;
	frmivalenum.index = 0;

	double fps = 0;

	while ((ret = xioctl(fd, VIDIOC_ENUM_FRAMEINTERVALS,
					&frmivalenum)) != -1)
	{
		if (frmivalenum.type == V4L2_FRMIVAL_TYPE_DISCRETE) {
			fps = (double)frmivalenum.discrete.denominator /
				(double)frmivalenum.discrete.numerator;
			if (fabs((double)default_fps - fps) < 0.1)
				break;
		} else if (frmivalenum.type == V4L2_FRMIVAL_TYPE_CONTINUOUS) {
		} else if (frmivalenum.type == V4L2_FRMIVAL_TYPE_STEPWISE) {
		} else {
		}
		frmivalenum.index++;
	}

	if (ret < 0)
		throw std::runtime_error("requested framerate not found");

	/*
	 * get current format settings
	 */

	struct v4l2_format format;
	memset(&format, 0, sizeof(format));

	struct v4l2_streamparm streamparm;
	memset(&streamparm, 0, sizeof(streamparm));

	format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (xioctl(fd, VIDIOC_G_FMT, &format.type) == -1)
		throw std::runtime_error("failed to get capturing format");

	streamparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (xioctl(fd, VIDIOC_G_PARM, &streamparm) == -1)
		throw std::runtime_error("failed to get streaming parameters");

	/*
	 * set desired format settings
	 */

	format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	format.fmt.pix.width = frmsizeenum.discrete.width;
	format.fmt.pix.height = frmsizeenum.discrete.height;
	format.fmt.pix.pixelformat = fmtdesc.pixelformat;
	format.fmt.pix.field = V4L2_FIELD_NONE;
	if (xioctl(fd, VIDIOC_S_FMT, &format) == -1)
		throw std::runtime_error("failed to set capturing format");

	streamparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	streamparm.parm.capture.capability |= V4L2_CAP_TIMEPERFRAME;
	streamparm.parm.capture.timeperframe.numerator =
		frmivalenum.discrete.numerator;
	streamparm.parm.capture.timeperframe.denominator =
		frmivalenum.discrete.denominator;
	if (xioctl(fd, VIDIOC_S_PARM, &streamparm) == -1)
		throw std::runtime_error("failed to set streaming parameters");

	/*
	 * request buffers
	 */

	struct v4l2_requestbuffers requestbuffers;
	memset(&requestbuffers, 0, sizeof(requestbuffers));

	requestbuffers.count = CAMERA_BUFFERS_COUNT;
	requestbuffers.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	requestbuffers.memory = V4L2_MEMORY_MMAP;
	if (xioctl(fd, VIDIOC_REQBUFS, &requestbuffers) == -1)
		throw std::runtime_error("failed to request video buffers");

	struct mapped_buffer {
		uint8_t *data;
		size_t length;
		mapped_buffer() :
			data{nullptr},
			length{0}
		{}
	};

	std::vector<mapped_buffer> buffers(requestbuffers.count);

	/*
	 * unmap buffers on exit
	 */
	auto buf_map_guard = ON_SCOPE_EXIT() {
		for (auto &b : buffers) {
			if (b.data && b.data != MAP_FAILED && b.length) {
        			munmap(b.data, b.length);
			}
		}
	};

	struct v4l2_buffer buffer;

	size_t buf_max_len = 0;
	for (size_t i = 0; i < buffers.size(); ++i) {
		memset(&buffer, 0, sizeof(buffer));

		buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buffer.memory = V4L2_MEMORY_MMAP;
		buffer.index = (__u32) i;

		if (xioctl(fd, VIDIOC_QUERYBUF, &buffer) == -1)
			throw std::runtime_error(
					"failed to query video buffer");

		if (buffer.length > buf_max_len)
			buf_max_len = buffer.length;

		buffers[i].length = buffer.length;
		buffers[i].data = (uint8_t *) mmap(NULL, buffer.length,
				PROT_READ | PROT_WRITE, MAP_SHARED, fd,
				buffer.m.offset);

		if (buffers[i].data == MAP_FAILED)
			throw std::runtime_error(
					"failed to map buffer");
	}

	/*
	 * enqueue buffers
	 */

	/* a buffer currently shared with the imgproc thread */
	__u32 shared_buf_idx = 0;

	/* queue all except the shared one */
	for (size_t i = 1; i < buffers.size(); ++i) {
		memset(&buffer, 0, sizeof(buffer));

		buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buffer.memory = V4L2_MEMORY_MMAP;
		buffer.index = (__u32) i;
		if (xioctl(fd, VIDIOC_QBUF, &buffer) == -1)
			throw std::runtime_error(
					"failed to enqueue buffer");
	}

	/*
	 * start image processing thread
	 */

	std::thread imgproc_thread(imgproc_worker, sec0);
	auto imgproc_thread_guard = ON_SCOPE_EXIT() {
		g_ipsig_mtx.lock();
		g_ipsig = imgproc_signal::quit;
		g_ipsig_mtx.unlock();
		g_ipsig_cv.notify_one();
		imgproc_thread.join();
	};

	/*
	 * start streaming
	 */

	enum v4l2_buf_type buf_type;

	buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (xioctl(fd, VIDIOC_STREAMON, &buf_type) == -1)
		throw std::runtime_error(
				"failed to set streaming on");

	auto streaming_guard = ON_SCOPE_EXIT() {
		buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (xioctl(fd, VIDIOC_STREAMOFF, &buf_type) == -1) {
			std::cerr << "cleanup error: "
				"failed to set streaming off" << std::endl;
		}
	};

	fd_set fds;
	struct timeval timeout;
	while (!g_quit) {
		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		timeout.tv_sec = 1; /* 1 sec ok? XXX */
		timeout.tv_usec = 0;
		ret = select(fd+1, &fds, NULL, NULL, &timeout);

		if (ret == -1)
			throw std::runtime_error("video frame timeout");
		if (ret == 0)
			continue;

		memset(&buffer, 0, sizeof(buffer));
		buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buffer.memory = V4L2_MEMORY_MMAP;
		if (xioctl(fd, VIDIOC_DQBUF, &buffer) == -1)
			throw std::runtime_error("failed to dequeue buffer");

		if (g_ipsig_mtx.try_lock()) {
			std::swap(buffer.index, shared_buf_idx);
			g_shared_buf = buffers[shared_buf_idx].data;
			g_shbuf_ts = buffer.timestamp;
			g_ipsig = imgproc_signal::process;
			g_ipsig_mtx.unlock();
			g_ipsig_cv.notify_one();
		}

		if (xioctl(fd, VIDIOC_QBUF, &buffer) == -1)
			throw std::runtime_error(
					"failed to reenqueue buffer");
	}
}
catch (std::exception &e) {
	std::cerr << "camera thread error: " << e.what() << std::endl;
}

void imgproc_worker(time_t sec0)
{
	/*
	 * if something goes wrong signal all threads to quit
	 */
	auto quit_guard = ON_SCOPE_EXIT() {
		g_quit = true;
	};

	static_assert(default_pixelformat == pixelformat_of('G','R','E','Y'),
			"imgproc thread: pixelformat is not supported");

	system("mkdir -p output");

	using namespace std::chrono;
	steady_clock::time_point t0, t1;
	milliseconds delay;

	std::unique_lock<std::mutex> lock(g_ipsig_mtx);
	t0 = steady_clock::now();
	for (;;) {
		while (g_ipsig == imgproc_signal::none)
			g_ipsig_cv.wait(lock);
		if (g_ipsig == imgproc_signal::quit)
			break;
		g_ipsig = imgproc_signal::none;

		long sec = (long) (g_shbuf_ts.tv_sec - sec0);
		long msec = (long) (g_shbuf_ts.tv_usec/1000);

		cv::Mat image(cv::Size(default_resolution_width,
					default_resolution_height),
				CV_8UC1, g_shared_buf);
		std::ostringstream ss;
		ss << "output/shot_" << std::setw(3) << std::setfill('0') << sec
			<< "_" << std::setw(3) << std::setfill('0') << msec
			<< ".png";
		cv::imwrite(ss.str(), image);

		g_shared_buf = nullptr;

		t1 = steady_clock::now();
		delay = milliseconds(1000)
			- duration_cast<milliseconds>(t1 - t0);
		std::this_thread::sleep_for(delay);
		t0 = steady_clock::now();
	}
}

int xioctl(int fd, unsigned long request, void *arg)
{
	int ret;
	int tries = 5;
	do {
		ret = ioctl(fd, request, arg);
	} while (ret && --tries && ((errno == EINTR) || (errno == EAGAIN)
				|| (errno == ETIMEDOUT)));

	return ret;
}

