
#include <iostream>
#include <vector>
#include <GL/glew.h>

/*
 * This function is taken from opengl-tutorial website with minor changes.
 */
GLuint load_shaders(char const *vtx_code, char const *fmt_code)
{
	//std::string vtx_code;
	//std::ifstream vtx_stream(vtx_file, std::ios::in);
	//if (vtx_stream.is_open()) {
	//	std::string line;
	//	while (getline(vtx_stream, line))
	//		vtx_code += line + "\n";
	//	vtx_stream.close();
	//} else {
	//	std::cout << "failed to open " << vtx_file << std::endl;
	//	return 0;
	//}

	//std::string fmt_code;
	//std::ifstream fmt_stream(fmt_file, std::ios::in);
	//if (fmt_stream.is_open()) {
	//	std::string line;
	//	while (getline(fmt_stream, line))
	//		fmt_code += line + "\n";
	//	fmt_stream.close();
	//} else {
	//	std::cout << "failed to open " << fmt_file << std::endl;
	//	return 0;
	//}

	GLuint vtx_id = glCreateShader(GL_VERTEX_SHADER);
	GLuint fmt_id = glCreateShader(GL_FRAGMENT_SHADER);

	GLint result = GL_FALSE;
	int length = 0;

	char const *vtx_ptr = vtx_code;//vtx_code.c_str();
	glShaderSource(vtx_id, 1, &vtx_ptr, NULL);
	glCompileShader(vtx_id);
	glGetShaderiv(vtx_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv(vtx_id, GL_INFO_LOG_LENGTH, &length);
	if (length > 0) {
		std::vector<char> msg(length);
		glGetShaderInfoLog(vtx_id, length, NULL, &msg[0]);
		std::cout << &msg[0] << std::endl;
	}
	if (result == GL_FALSE) {
		std::cout << "failed to compile vertex shader" << std::endl;
		return 0;
	}

	char const *fmt_ptr = fmt_code;//fmt_code.c_str();
	glShaderSource(fmt_id, 1, &fmt_ptr, NULL);
	glCompileShader(fmt_id);
	glGetShaderiv(fmt_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fmt_id, GL_INFO_LOG_LENGTH, &length);
	if (length > 0) {
		std::vector<char> msg(length);
		glGetShaderInfoLog(fmt_id, length, NULL, &msg[0]);
		std::cout << &msg[0] << std::endl;
	}
	if (result == GL_FALSE) {
		std::cout << "failed to compile fragment shader" << std::endl;
		return 0;
	}

	GLuint pgm_id;
	pgm_id = glCreateProgram();
	glAttachShader(pgm_id, vtx_id);
	glAttachShader(pgm_id, fmt_id);
	glLinkProgram(pgm_id);

	glGetProgramiv(pgm_id, GL_LINK_STATUS, &result);
	glGetProgramiv(pgm_id, GL_INFO_LOG_LENGTH, &length);
	if (length > 0) {
		std::vector<char> msg(length);
		glGetProgramInfoLog(pgm_id, length, NULL, &msg[0]);
		std::cout << &msg[0] << std::endl;
	}
	if (result == GL_FALSE) {
		std::cout << "failed to link shader program" << std::endl;
		return 0;
	}

	glDetachShader(pgm_id, vtx_id);
	glDetachShader(pgm_id, fmt_id);

	glDeleteShader(vtx_id);
	glDeleteShader(fmt_id);

	return pgm_id;
}

