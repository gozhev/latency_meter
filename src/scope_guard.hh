#ifndef _SCOPE_GUARD_HH_
#define _SCOPE_GUARD_HH_

#include <utility>

template<typename T>
class scope_guard {
	T f_;
	bool active_;
public:
	scope_guard(T f):
		f_(std::move(f)),
		active_(true)
	{}
	~scope_guard() { if (active_) f_(); }
	void dismiss() { active_ = false; }
	scope_guard() = delete;
	scope_guard(scope_guard const &) = delete;
	scope_guard &operator=(scope_guard const &) = delete;
	scope_guard(scope_guard &&rhs):
		f_(std::move(rhs.f_)),
		active_(rhs.active_)
	{ rhs.dismiss(); }
};

enum class scope_guard_on_exit {};

template<typename F>
scope_guard<F> operator+(scope_guard_on_exit, F f)
{
	return scope_guard<F>(std::move(f));
}

template<class F>
scope_guard<F> scope_guard_of(F f)
{
	return scope_guard<F>(std::move(f));
}

#define ON_SCOPE_EXIT() scope_guard_on_exit() + [&]()

template<class T, class F>
std::unique_ptr<T, F> unique_ptr_of(T *p, F f)
{
	return std::unique_ptr<T, F>(p, std::move(f));
}

#endif

