#ifndef _LOAD_SHADERS_HH_
#define _LOAD_SHADERS_HH_

#include <GL/glew.h>

GLuint load_shaders(char const *vtx_code, char const *fmt_code);

#endif

