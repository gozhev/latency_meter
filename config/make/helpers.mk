# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

#
# As long as the following variables might be included at the most end of the
# file they should only be used in receipts.
#

define rm_all
	rm -f $1
endef

define rm_empty_dirs
	$(foreach x, $1,\
		find $x -type d -empty -delete 2>/dev/null ||:)
endef

define mkdir_cond
	$(if $(wildcard $1),,$(strip\
		mkdir $1))
endef

define mkdirtree_cond
	$(if $(wildcard $1),,$(strip\
		mkdir -p $1))
endef

define NL


endef

