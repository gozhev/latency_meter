# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

# where to put object files
ifdef O
ifeq ("$(origin O)", "command line")
BUILD_DIR := $(abspath $(O))
DEST_DIR := $(abspath $(O))
endif
endif

# where to put target binaries
ifdef D
ifeq ("$(origin D)", "command line")
DEST_DIR := $(abspath $(D))
endif
endif

