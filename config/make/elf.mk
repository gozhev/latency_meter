# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

ELF ?= $(BUILD_DIR)/$(NAME).elf

ELF_DEPS += $(OBJS)
ELF_DEPS += $(STATIC_LIBS)

$(ELF): $(ELF_DEPS)
	$(strip\
	$(LD) -o $@ $(LDOBJS) $(STATIC_LIBS) $(LDFLAGS) $(LDLIBS))

.PHONY: elf
elf: $(ELF)

#
# dir handling
#
ELF_DIR := $(dir $(ELF))
$(ELF): | $(ELF_DIR)

# global hook
DIRTREES += $(ELF_DIR)

#
# cleanup
#
.PHONY: elf-clean
elf-clean:
	$(call rm_all, $(ELF))

# global hook
clean distclean: objs-clean elf-clean

