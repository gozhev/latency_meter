# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

# this line disables builtin rules
.SUFFIXES:

.DEFAULT_GOAL := all

.PHONY: all
all:

.PHONY: clean distclean
clean distclean:

# handle build dir here
DIRS += $(BUILD_DIR)

