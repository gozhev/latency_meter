# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

#
# single directories
#
$(sort $(DIRS)):
	$(call mkdir_cond, $@)

#
# directories and its parent directories and so on
#
$(sort $(DIRTREES)):
	$(call mkdirtree_cond, $@)

.PHONY: dirs-clean
dirs-clean:
	$(call rm_empty_dirs, $(DIRS))

.PHONY: dirtrees-clean
dirtrees-clean:
	$(call rm_empty_dirs, $(DIRTREES))

clean distclean: dirs-clean

