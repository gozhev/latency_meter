# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

CXX_EXTS := .cc .cpp .cxx .c++
CXX_OBJS := $(call get_obj_by_ext, $(CXX_EXTS))

$(CXX_OBJS): $(BUILD_DIR)/%.o: %
	$(CXX) -c -o $@ $(CXXFLAGS) $(CPPFLAGS) $<


