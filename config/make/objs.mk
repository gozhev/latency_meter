# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

#
# helper functions
#
define get_obj_by_ext
$(filter $(patsubst %,\%%.o,$1), $(OBJS))
endef

define src_to_obj
$(patsubst %, $(BUILD_DIR)/%.o, $1)
endef

#
# obj handling
#

# all objects
OBJS := $(call src_to_obj, $(SRCS))
_OBJS_EXCL := $(call src_to_obj, $(DO_NOT_LINK)) $(DO_NOT_LINK)
# objects that should be passed to the linker explicitely
LDOBJS := $(filter-out $(_OBJS_EXCL), $(OBJS))

#
# we don't bother if some object are lost but we don't want them to be
# removed forcibly
#
.SECONDARY: $(OBJS)

.PHONY: objs
objs: $(OBJS)

#
# dir handling
#
OBJ_DIRS := $(sort $(dir $(OBJS)))

$(OBJS): | $(OBJ_DIRS)
$(OBJ_DIRS): | $(BUILD_DIR)

# global hook
DIRTREES += $(OBJ_DIRS)

#
# cleanup
#
.PHONY: objs-clean
objs-clean:
	$(call rm_all, $(OBJS))

# global hooks
clean distclean: objs-clean

