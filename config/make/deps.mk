# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

#
# global input
#
DEPS := $(addsuffix .d, $(OBJS))

#
# local state
#
GENDEP_FLAGS = -MMD -MP -MT $@ -MF $@.d

-include $(DEPS)

$(DEPS): ;

.PHONY: deps-clean
deps-clean:
	$(call rm_all, $(DEPS))

#
# global output
#
CPPFLAGS += $(GENDEP_FLAGS)

$(OBJS): %: %.d

clean distclean: deps-clean

