# © Mikhail Gozhev <dev@gozhev.ru> / November 2018 / Moscow, Russia

BUILD_DIR ?= build
DEST_DIR ?= .
include config/make/args.mk

NAME := latency_meter

SRCS :=\
	src/main.cc\
	src/load_shaders.cc

# c preprocessor
CPPFLAGS +=\
	-Isrc

CXXFLAGS +=\
	-std=c++11\
	-pthread

CXXFLAGS +=\
	-Wall\
	-Wextra\
	-pedantic\
	-Wformat=2\
	-Wshadow\
	-Wwrite-strings\
	-Wundef\
	-Wconversion\
	-Wlogical-op

LDFLAGS +=\
	-lstdc++\
	-pthread

CXX := g++
LD := g++

# opencv
CPPFLAGS += $(patsubst -I%,-isystem %,$(shell pkg-config --cflags opencv))
LDFLAGS += $(shell pkg-config --libs opencv) -lcblas

# opengl
CPPFLAGS += -isystem /usr/include/GLFW
LDFLAGS += -lGL -lglfw -lGLEW

all: elf
ELF := $(DEST_DIR)/$(NAME)

include config/make/helpers.mk
include config/make/build.mk
include config/make/objs.mk
include config/make/elf.mk
include config/make/cxx.mk
include config/make/deps.mk
include config/make/dirs.mk

